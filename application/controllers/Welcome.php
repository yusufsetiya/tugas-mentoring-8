<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Welcome extends CI_Controller
{


	public function index()
	{
		$data['error_message'] = " ";
		$this->load->view('index', $data);
	}

	public function exportToExcel()
	{

		// Buat objek Spreadsheet
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$sheet->getStyle('A1')->getFont()->setBold(true);
		$sheet->getStyle('A1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$sheet->getColumnDimension('A')->setWidth(20);
		$sheet->getColumnDimension('B')->setWidth(35);
		$sheet->mergeCells('A1:B1');
		$sheet->setCellValue('A1', 'Data Mahasiswa');
		$sheet->setCellValue('A3', 'Nama');
		$sheet->setCellValue('A4', 'Fakultas');
		$sheet->setCellValue('A5', 'Prodi');
		$sheet->setCellValue('A6', 'Nomor Telepon');
		$sheet->setCellValue('A7', 'Jenis Kelamin');
		$sheet->setCellValue('A8', 'Alamat');
		$sheet->setCellValue('A9', 'Tanggal Lahir');
		$sheet->setCellValue('B3', 'Mochamad Yusuf Setiya Putra');
		$sheet->setCellValue('B4', 'Fakultas Teknologi Informasi');
		$sheet->setCellValue('B5', 'S1 Sistem Informasi');
		$sheet->setCellValue('B6', '087721015398');
		$sheet->setCellValue('B7', 'Laki-Laki');
		$sheet->setCellValue('B8', 'Jl. Semanggi No. 11');
		$sheet->setCellValue('B9', '7 September 2002');

		$writer = new Xlsx($spreadsheet);

		// Menyimpan file Excel
		$file_name = 'Biodata Mahasiswa.xlsx';
		$writer->save($file_name);

		// Set header HTTP untuk mengunduh file
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $file_name . '"');
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
	}

	public function import()
	{
		$config['upload_path'] = './public/excel';
		$config['allowed_types'] = 'xls|xlsx|csv';
		$config['max_size'] = 10000;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('file')) {
			$data = $this->upload->data();

			// validate file extension
			$objPHPExcel = null;

			if ($data['file_ext'] == '.xls') {
				$objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xls');
			} else if ($data['file_ext'] == '.xlsx') {
				$objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
			} else if ($data['file_ext'] == '.csv') {
				$objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Csv');
			}

			if ($objPHPExcel == null) {
				echo "Invalid file extension";
				return;
			}

			// read file
			$objPHPExcel = $objPHPExcel->load($data['full_path']);
			$sheet = $objPHPExcel->getActiveSheet();

			// get max row
			$highestRow = $sheet->getHighestRow();

			// get max column
			$highestColumn = $sheet->getHighestColumn();

			$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

			// validate cell A3 must be 'Nama'
			if ($sheet->getCell('A3')->getValue() != 'Nama') {
				// echo "Invalid file format, cell A1 must be 'Nama Mahasiswa'";
				$data['error_message'] = "<div class='alert alert-danger'>Invalid file format, cell A1 must be 'Nama'</div>";
				$this->load->view('index', $data);
				unlink($data['full_path']);
				return;
			}

			// validate cell A4 must be 'Fakultas'
			if ($sheet->getCell('A4')->getValue() != 'Fakultas') {
				// echo "Invalid file format, cell A2 must be 'Fakultas'";
				$data['error_message'] = "<div class='alert alert-danger'>Invalid file format, cell A2 must be 'Fakultas'</div>";
				$this->load->view('index', $data);
				unlink($data['full_path']);
				return;
			}

			// validate cell A5 must be 'Prodi'
			if ($sheet->getCell('A5')->getValue() != 'Prodi') {
				// echo "Invalid file format, cell A3 must be 'Prodi'";
				$data['error_message'] = "<div class='alert alert-danger'>Invalid file format, cell A3 must be 'Prodi'</div>";
				$this->load->view('index', $data);
				unlink($data['full_path']);
				return;
			}

			// validate cell A6 must be 'Nomor Telepon'
			if ($sheet->getCell('A6')->getValue() != 'Nomor Telepon') {
				// echo "Invalid file format, cell A4 must be 'No telpon'";
				$data['error_message'] = "<div class='alert alert-danger'>Invalid file format, cell A4 must be 'Nomor Telpon'</div>";
				$this->load->view('index', $data);
				unlink($data['full_path']);
				return;
			}

			// validate cell A7 must be 'Jenis kelamin'
			if ($sheet->getCell('A7')->getValue() != 'Jenis Kelamin') {
				// echo "Invalid file format, cell A5 must be 'Jenis kelamin'";
				$data['error_message'] = "<div class='alert alert-danger'>Invalid file format, cell A5 must be 'Jenis kelamin'</div>";
				$this->load->view('index', $data);
				unlink($data['full_path']);
				return;
			}

			// validate cell A8 must be 'Alamat'
			if ($sheet->getCell('A8')->getValue() != 'Alamat') {
				// echo "Invalid file format, cell A6 must be 'Alamat'";
				$data['error_message'] = "<div class='alert alert-danger'>Invalid file format, cell A6 must be 'Alamat'</div>";
				$this->load->view('index', $data);
				unlink($data['full_path']);
				return;
			}

			// validate cell A9 must be 'Tanggal Lahir'
			if ($sheet->getCell('A9')->getValue() != 'Tanggal Lahir') {
				// echo "Invalid file format, cell A7 must be 'Tanggal Lahir'";
				$data['error_message'] = "<div class='alert alert-danger'>Invalid file format, cell A7 must be 'Tanggal Lahir'</div>";
				$this->load->view('index', $data);
				unlink($data['full_path']);
				return;
			}

			// get data
			$realData = [];

			for ($row = 1; $row <= $highestRow; ++$row) {
				for ($col = 1; $col <= $highestColumnIndex; ++$col) {
					$realData[$row][] = $sheet->getCell(\PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col) . $row)->getValue();
				}
			}

			// save file as 'public/excel/data_excel.xlsx'
			$writer = new Xlsx($objPHPExcel);
			$writer->save('public/excel/data_excel.xlsx');

			// delete uploaded file
			unlink($data['full_path']);

			// show data
			$this->load->view('viewExcel', ['data' => $realData]);
		} else {
			echo $this->upload->display_errors();
		}
	}

	private function getData()
	{
		// import data 
		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('public/excel/data.xlsx');

		// get active sheet
		$activeSheet = $spreadsheet->getActiveSheet();

		// get max row
		$highestRow = $activeSheet->getHighestRow();

		// get max column
		$highestColumn = $activeSheet->getHighestColumn();

		$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

		// get data
		$data = [];

		for ($row = 1; $row <= $highestRow; ++$row) {
			for ($col = 1; $col <= $highestColumnIndex; ++$col) {
				$data[$row][] = $activeSheet->getCell(\PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col) . $row)->getValue();
			}
		}

		return $data;
	}
}
