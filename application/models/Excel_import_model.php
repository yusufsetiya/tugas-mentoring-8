<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;

class Excel_import_model extends CI_Model
{

    public function import_data($file_path)
    {
        $imported_data = array();

        try {
            $spreadsheet = IOFactory::load($file_path);
            $worksheet = $spreadsheet->getActiveSheet();
            $rows = $worksheet->toArray();

            // Hapus header (jika ada)
            unset($rows[0]);

            // Proses data yang diimpor
            foreach ($rows as $row) {
                $imported_data[] = $row;
            }

            return $imported_data;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
