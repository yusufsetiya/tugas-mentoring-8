<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Praktek PHP Excel</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <div class="card mt-5">
            <h5 class="card-header text-center">Tugas Mentoring 8 Praktek | PHP Excel</h5>
            <div class="card-body">
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Export Master File Disini</label><br>
                    <a href="<?php echo site_url('Welcome/exportToExcel'); ?>" class="btn btn-success">Ekspor ke Excel</a>
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Import File Rubahan</label>
                    <form action="<?= site_url('welcome/import'); ?>" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-10">
                                <input type="file" class="form-control" name="file" accept=".xls,.xlsx,.csv" />
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-success" type="submit">Import</button>
                            </div>
                            <div class="container mt-3">
                                <?php echo $error_message; ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

</html>